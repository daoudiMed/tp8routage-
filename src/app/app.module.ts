import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms' ;
import { RouterModule } from '@angular/router' ;

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InventionsComponent } from './inventions/inventions.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    InventionsComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule, 
    FormsModule,
    AppRoutingModule,

    RouterModule.forRoot([
      {
      path: '' ,
      redirectTo: 'inventions' ,
      pathMatch: 'full'
      },
      {
      path: 'inventions' ,
      component: InventionsComponent
      } ,
      {
      path: 'details/:id' ,
      component: DetailsComponent
      }
     ]),
    ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
